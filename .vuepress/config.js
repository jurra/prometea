module.exports = {
  title: "Prometea",
  dest: "public",
  base: "/hax-app/",


  themeConfig: {
    logo: 'logo.png',
    // projectLogo: "/logo.png",
    projectTitle: "Polemidos",

    nav: [
          { text: 'Product', link: '/' },
          { text: 'Team', link: '/team' },
          { text: 'Market Validation', link: '/market/' },
          { text: 'Plan', link: '/plan/' }
        ],

        sidebar: 'auto'

      }

};
