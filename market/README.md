---
sidebar: true
---

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRIT8uqN6zgcWku4jMMEyw0_7mfFxaAsd0u1jLcmohl5-kFtbfNrZZHeBs9dkJPsp46SRzso-Vyovr5/embed?start=false&loop=false&delayms=3000" frameborder="0" width="100%" height="500" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

## What potential customers say about our product

**100% of our customers** are looking for an accessible plasma cutter kit in the range of $1000-$2500

**76% wants this machine** to build custom parts they need for their projects

**70% wants to see the  Bill of Materials** so that they know what they are paying for

**84% wants an open source product** to study and hack the technology

There is over 1000 The Fab Lab Network is an open, creative community of fabricators, artists, scientists, engineers, educators, students, amateurs, professionals, ages 5 to 75+, located in more than 100 countries in over 1,000 Fab Labs. The platform is a curated, interactive directory of these locations.


> Over the past decade, makerspaces have exploded in popularity all over the globe—user-reported numbers show nearly 1,400 active spaces, 14 times as many as in 2006. Also called hackerspaces or innovation labs, these establishments act as communal workshops where makers can share ideas and tools. They can pop up anywhere, including in schools, libraries, and community centers. Different locations offer different resources, ranging from 3D printers to synthetic biology kits.


> China’s makerspace numbers at 5,500, with a predicted increase to 11,640 by 2021.

**Read more:**[China maker spaces](https://www.sixthtone.com/news/1003171/made-in-china-the-boom-and-bust-of-makerspaces)



> The global metal cutting machine (MCM) market size was estimated at USD 5.99 billion in 2016. Increasing global demand from several application industries to cut various industry components with high precision level and edge quality is expected to be a key growth factor. In addition, growing automotive industry in Asia Pacific is anticipated to fuel the demand over the forecast period.

**Read more:** [Metal cutting market size](https://www.grandviewresearch.com/industry-analysis/metal-cutting-machine-market)



[Number of makerspaces worldwide](https://www.popsci.com/rise-makerspace-by-numbers)

[Maker Market study](https://cdn.makezine.com/make/bootstrap/img/etc/Maker-Market-Study.pdf)

> Roughly half of American adults call themselves makers. “There’s an increased awareness of how broad making can be and how inclusive it can be,” says Steve Davee, director of education at the nonprofit Maker Ed. “But really, makerspaces in other forms have existed for a huge amount of time: We just called them woodshops, home-ec centers, model shops, and computer labs.”


***
# Metal cutting market insights
> Plasma cutting machines became one of the most vital cutting tools among other non-conventional machine processes. Its ability to shear through metal sheets and tubing with high thickness made the machines one of the most preferred in automotive, industrial manufacturing, aerospace and HVAC industries.

> Asia Pacific is the leading region in terms of market value in global plasma cutting machines market. Developing countries in Asia Pacific such as China, India, and South Korea are consistently boosting their manufacturing capabilities to match optimum product quality. The aforementioned countries are extensively incorporation non-conventional machining processes including plasma cutting machines. Continuously developing industrial manufacturing and automotive sector expected to help Asia Pacific to retain its dominant position plasma cutting machines market.

> Competitive Insights:
Overall competitive landscape of plasma cutting machines market is highly fragmented with leading machine tools and control system manufacturers face-off each other for market share. Developing countries across the globe are providing numerous growth opportunities to these companies. However, the companies have to keep a check on changing market trends and should be ready to incorporate advanced technology.

[Plasma cutting market insights](https://www.reuters.com/brandfeatures/venture-capital/article?id=63768)
